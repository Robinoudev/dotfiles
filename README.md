# Upgrading
```sh
# nix-channel --update
# nixos-rebuild switch --upgrade

for nix-env packages

$ nix-env -u
```

# Flakes
```sh
nix flake update
```

# TODO
- [ ] Change home-manager setup to only use it for dotfiles, not for packages
