# This gives me the ability to set optional arguments with defaults
# Well, we already had that, but this makes it a little less painful :)
{ lib, ... }:

let
  inherit (lib) mkOption types;
in
rec {
  mkOpt = type: default:
    mkOption { inherit type default; };

  mkOpt' = type: default: description:
    mkOption { inherit type default description; };

  mkBoolOpt = default: mkOption {
    inherit default;
    type = types.bool;
    example = true;
  };
}
