C = {}

C.terminal = "alacritty"
C.editor = "nvim"
C.modkey = "Mod4"
C.browser = "brave"

require "configs.binds"
