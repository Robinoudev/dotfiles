#/bin/bash
#
for DIR in alacritty awesome bspwm doom dunst fontconfig git gtk-2.0 gtk-3.0 kitty newsboat nvim pam.d picom rofi sxhkd systemd tmux xdg xmonad zsh xdg i3
do
	mkdir -p $HOME/.config/$DIR
	stow -v $DIR -t $HOME/.config/$DIR
done
