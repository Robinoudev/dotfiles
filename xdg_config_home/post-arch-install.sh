#!/bin/bash

set -e

function log {
  local LINE="[arch-linux-install] $*"
  echo "$LINE"
  echo "${LINE//?/-}"
}

function ask {
  read -p "$1> "
  eval "export $2=\$REPLY"
}

log "Installing dotfiles"
sudo -u robin mkdir /home/robin/code
sudo -u robin git clone https:://github.com/robinoudev/dotfiles ~/code/dotfiles
pushd /home/robin/code/dotfiles
git submodule update --init --recursive
./install.sh
popd

log "Setting up paru AUR helper"
mkdir -p /home/robin/.local/programs
git clone https://aur.archlinux.org/paru.git ~/.local/programs
pushd /home/robin/.local/programs/paru
makepkg --noconfirm -si
popd

log "Setting up the firewall"
pacman -S --noconfirm ufw
sudo systemctl enable ufw.service
sudo ufw default deny
sudo ufw allow from 192.168.0.0/24
sudo ufw limit ssh
sudo ufw enable

log "Installing ALL packages I use (could take a while)"
pacman -Syyu
sudo -u robin paru -S --noconfirm --needed base-devel nerd-fonts-ibm-plex-mono \
  zsh rofi i3-gaps i3status i3lock-fancy firefox picom alacritty unzip \
  ttf-iosevka ttf-joypixels pipewire pipewire-pulse wireplumber stow \
  openssh fd xorg-xbacklight exa ripgrep asdf-vm dunst xorg-xset i3blocks \
  otf-font-awesome-5-free feh

log "Enabling sshd"
systemctl start sshd.service
systemctl enable sshd.service
sudo -u robin systemctl --user enable ssh-agent.sevice

log "Change shell to zsh"
chsh --shell /bin/zsh robin

log "Installing asdf packages"
sudo -u robin asdf plugin-add ruby
sudo -u robin asdf plugin-add nodejs
sudo -u robin asdf install ruby 3.0.3
sudo -u robin asdf global ruby 3.0.3

log "Setting up neovim"
sudo -u robin paru -S --noconfirm --needed cmake unzip ninja tree-sitter curl
git clone https://github.com/neovim/neovim /home/robin/.local/programs/neovim
pushd /home/robin/.local/programs/neovim && make -j4
make install
popd
git clone --depth 1 https://github.com/wbthomason/packer.nvim\
 ~/.local/share/nvim/site/pack/packer/start/packer.nvim
nvim +PackerInstall

pushd .local/share/nvim/site/pack/packer/start/command-t/ruby/command-t/ext/command-t
ruby extconf.rb
make
popd

log "Setting up doom emacs"

log "setting up bluetooth"

log "Setting up real-prog-dvorak"
