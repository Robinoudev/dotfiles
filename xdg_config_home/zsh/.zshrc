# Install zgenom when not installed
if [ ! -d "$HOME/.local/share/zgenom" ]; then
  echo "Installing jandamm/zgenom"
  git clone https://github.com/jandamm/zgenom "$HOME/.local/share/zgenom"
fi

source $HOME/.local/share/zgenom/zgenom.zsh
zgenom autoupdate

if ! zgenom saved; then
  zgenom load junegunn/fzf shell
  zgenom load jeffreytse/zsh-vi-mode
  zgenom load zdharma-continuum/fast-syntax-highlighting
  zgenom load zsh-users/zsh-completions src
  zgenom load zsh-users/zsh-autosuggestions
  zgenom load zsh-users/zsh-history-substring-search
  zgenom load hlissner/zsh-autopair autopair.zsh

  zgenom save
  zgenom compile $ZDOTDIR
fi


# OH MY ZSH
# export ZSH="$HOME/.oh-my-zsh"
# ZSH_THEME="robbyrussell"
# plugins=(git)

# Prompt
git_branch() {
    if out="$(git -C . rev-parse > /dev/null 2>&1)"; then
        printf " $%s$(git branch | pcregrep -o1 "\*[\s]*(.*)")"
    fi
}

set_prompt() {
    branch="$(git_branch)"
    NEWLINE=$'\n'
    PROMPT="%F{white}┌[%f%F{blue}%~%f%F{white}]%f${NEWLINE}%F{white}└[%f%F{green}%n%f%F{yellow}@%f%F{blue}%m%f%F{red}%}${branch}%F{white}]:%f "
}

precmd_functions+=(set_prompt)
set_prompt

# setup completion
fpath=($ZDOTDIR/completions $fpath)

autoload -U compinit
compinit -u -d $HOME/.cache/zsh/zcompdump

# general settings
source $ZDOTDIR/config.zsh

# plugins
#
# NOTE: must come before zsh-history-substring-search & zsh-syntax-highlighting.
autoload -U select-word-style
select-word-style bash # only alphanumeric chars are considered WORDCHARS

ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE='fg=59'
ZSH_AUTOSUGGEST_STRATEGY=(match_prev_cmd completion)
HISTORY_SUBSTRING_SEARCH_ENSURE_UNIQUE=1

source $ZDOTDIR/keybinds.zsh
source $ZDOTDIR/completion.zsh
source $ZDOTDIR/aliases.zsh
source $ZDOTDIR/functions
source $ZDOTDIR/vars
source $ZDOTDIR/hash

if [ -e /etc/motd ]; then
  if ! cmp -s $HOME/.hushlogin /etc/motd; then
    tee $HOME/.hushlogin < /etc/motd
  fi
fi

test -f $HOME/.cargo/env && . $HOME/.cargo/env

alias luamake=/home/robin/.local/programs/lua-language-server/3rd/luamake/luamake

# Load asdf when available
[ -f $HOME/.asdf/asdf.sh ] && . $HOME/.asdf/asdf.sh
if command -v brew > /dev/null; then
  . $(brew --prefix asdf)/libexec/asdf.sh
fi

# Load direnv
eval "$(direnv hook zsh)"

# Load starship prompt
# eval "$(starship init zsh)"

# load local zsh rc
[ -f $HOME/.zshrc.local ] && source $HOME/.zshrc.local

# source $ZSH/oh-my-zsh.sh
