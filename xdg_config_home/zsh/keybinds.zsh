## vi-mode ###############
bindkey -v
bindkey -M viins 'jk' vi-cmd-mode
bindkey -M viins ' ' magic-space

# History search multi word
bindkey "^R" history-search-multi-word

# zsh-autosuggestions accept
bindkey '^f' autosuggest-accept

autoload -U edit-command-line
zle -N edit-command-line
bindkey '^x^x' edit-command-line

# have ctrs + left/right move over a word
bindkey "^[[1;5C" forward-word
bindkey "^[[1;5D" backward-word

# Shift + Tab
bindkey -M viins '^[[Z' reverse-menu-complete
# bind UP and DOWN arrow keys
bindkey '^[[A' history-substring-search-up
bindkey '^[[B' history-substring-search-down

# Make CTRL-Z background things and unbackground them.
function fg-bg() {
  if [[ $#BUFFER -eq 0 ]]; then
    fg
  else
    zle push-input
  fi
}
zle -N fg-bg
bindkey '^Z' fg-bg
