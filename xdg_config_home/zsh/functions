#vim:ft=sh

# quickly create a scratch directory
function scratch() {
  local SCRATCH=$(mktemp -d)
  echo 'Spawing subshell in scratch directory:'
  echo "  $SCRATCH"
  (cd $SCRATCH; zsh)
  echo "Removing scratch directory"
  rm -rf "$SCRATCH"
}

# fix term info for ssh
function ssh() {
  emulate -L zsh

  local LOCAL_TERM=$(echo -n "$TERM" | sed -e s/tmux/screen/)
  env TERM=$LOCAL_TERM ssh "$@"
}

# function tmux() {
#   emulate -L zsh
#
#   TMUX="tmux -f $TMUX_HOME/tmux.conf"
#
#   # Make sure even pre-existing tmux sessions use the latest SSH_AUTH_SOCK.
#   # (Inspired by: https://gist.github.com/lann/6771001)
#   local SOCK_SYMLINK=~/.ssh/ssh_auth_sock
#   if [ -r "$SSH_AUTH_SOCK" -a ! -L "$SSH_AUTH_SOCK" ]; then
#     ln -sf "$SSH_AUTH_SOCK" $SOCK_SYMLINK
#   fi
#
#   # If provided with args, pass them through.
#   if [[ -n "$@" ]]; then
#     env SSH_AUTH_SOCK=$SOCK_SYMLINK TMUX "$@"
#     return
#   fi
#
#   # Check for .tmux file (poor man's Tmuxinator).
#   if [ -x .tmux ]; then
#     # Prompt the first time we see a given .tmux file before running it.
#     local DIGEST="$(openssl sha512 .tmux)"
#     if ! grep -q "$DIGEST" ~/..tmux.digests 2> /dev/null; then
#       cat .tmux
#       read -k 1 -r \
#         'REPLY?Trust (and run) this .tmux file? (t = trust, otherwise = skip) '
#       echo
#       if [[ $REPLY =~ ^[Tt]$ ]]; then
#         echo "$DIGEST" >> ~/..tmux.digests
#         ./.tmux
#         return
#       fi
#     else
#       ./.tmux
#       return
#     fi
#   fi
#
#   # Attach to existing session, or create one, based on current directory.
#   local SESSION_NAME=$(basename "${$(pwd)//[.:]/_}")
#   env SSH_AUTH_SOCK=$SOCK_SYMLINK tmux -f $TMUX_HOME/tmux.conf new -A -s "$SESSION_NAME"
# }

# `git` wrapper:
#
#     - `git` with no arguments = `git status`; run `git help` to show what
#       vanilla `git` without arguments would normally show.
#     - `git root` = `cd` to repo root.
#     - `ROOT=$(git root)` = no args and stdout is not a tty; prints the root.
#     - `git root ARG...` = evals `ARG...` from the root (eg. `git root ls`).
#     - `git ARG...` = behaves just like normal `git` command.
#
function git() {
  if [ $# -eq 0 ]; then
    command git status
  elif [ "$1" = root ]; then
    shift
    local ROOT
    if [ "$(command git rev-parse --is-inside-git-dir 2> /dev/null)" = true ]; then
      if [ "$(command git rev-parse --is-bare-repository)" = true ]; then
        ROOT="$(command git rev-parse --absolute-git-dir)"
      else
        # Note: This is a good-enough, rough heuristic, which ignores
        # the possibility that GIT_DIR might be outside of the worktree;
        # see:
        # https://stackoverflow.com/a/38852055/2103996
        ROOT="$(command git rev-parse --git-dir)/.."
      fi
    else
      # Git 2.13.0 and above:
      ROOT="$(command git rev-parse --show-superproject-working-tree 2> /dev/null)"
      if [ -z "$ROOT" ]; then
        ROOT="$(command git rev-parse --show-toplevel 2> /dev/null)"
      fi
    fi
    if [ -z "$ROOT" ]; then
      ROOT="$PWD"
    fi
    if [ $# -eq 0 ]; then
      if [ -t 1 ]; then
        cd "$ROOT"
      else
        echo "$ROOT"
      fi
    else
      (cd "$ROOT" && eval "$@")
    fi
  else
    command git "$@"
  fi
}
