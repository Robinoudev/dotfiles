alias q="exit"

if (( $+commands[exa] )); then
  alias exa="exa --group-directories-first --git";
  alias l="exa -blF";
  alias ll="exa -abghilmu";
  alias llm='ll --sort=modified'
  alias la="LC_COLLATE=C exa -ablF";
  alias tree='exa --tree'
fi

if command -v nvim >/dev/null; then
  alias vim="nvim";
fi
alias v="vim";

# docker
alias dc="docker-compose"

# Tmux
# alias tmux="tmux -f $TMUX_HOME/tmux.conf";

# git
# g() { [[ $# = 0 ]] && git status --short . || git $*; }

# alias git='noglob git'
alias gfa='git fetch --prune --all'

alias shutdown='sudo /bin/shutdown -p now'
alias reboot='sudo /bin/shutdown -r now'

# newsboat
alias newsboat="newsboat -u $XDG_CONFIG_HOME/newsboat/urls"

# emacs
alias emacs="emacsclient -c -a 'emacs'"
alias edaemon="/bin/emacs --daemon"
alias doom="$HOME/.emacs.d/bin/doom"
ekill() { emacsclient --eval '(kill-emacs)'; }
e() { pgrep emacs && emacsclient -n "$@" || emacs -nw "$@" }
ediff() { emacs -nw --eval "(ediff-files \"$1\" \"$2\")"; }
eman()  { emacs -nw --eval "(switch-to-buffer (man \"$1\"))"; }
