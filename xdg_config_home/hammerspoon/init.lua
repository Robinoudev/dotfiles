-- launch kitty with cmd shift return
hs.hotkey.bind({"cmd", "shift"}, "Return", function()
  hs.application.launchOrFocus("iTerm")
end)

-- launch chromium with cmd shift w
hs.hotkey.bind({"cmd", "shift"}, "w", function()
  hs.application.launchOrFocus("Firefox")
end)

-- launch emacs with cmd shift w
hs.hotkey.bind({"cmd", "shift"}, "h", function()
  hs.application.launchOrFocus("Emacs")
end)

-- launch slack with cmd shift w
hs.hotkey.bind({"cmd", "shift"}, "s", function()
  hs.application.launchOrFocus("Slack")
end)

-- Optional configuration of beginEditShellCommand
-- spoon.editWithEmacs.openEditorShellCommand = "EDITOR -e '(hammerspoon-edit-begin)'"

hs.loadSpoon("editWithEmacs")
if spoon.editWithEmacs then
   local bindings = {
      edit_selection =  { {"alt"}, "1"},
      edit_all       = { {"alt"}, "2"}
   }
   spoon.editWithEmacs:bindHotkeys(bindings)
end
