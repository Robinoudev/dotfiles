require('rose-pine').setup({
    disable_background = true
})

vim.o.termguicolors = true

function ColorMeDaddy(color)
    color = color or "rose-pine"
    vim.cmd.colorscheme(color)
    vim.api.nvim_set_hl(0, "Normal", { bg = "none" })
    vim.api.nvim_set_hl(0, "NormalFloat", { bg = "none" })
end


ColorMeDaddy()
