local actions = require'lir.actions'
local mark_actions = require 'lir.mark.actions'
local clipboard_actions = require'lir.clipboard.actions'

require("lir").setup({
    show_hidden_files = true,
    ignore = {}, -- { ".DS_Store" "node_modules" } etc.
    devicons = {
      enable = true
    },
    float = { winblend = 15 },

    mappings = {
    ["<CR>"] = actions.edit,
    ["-"] = actions.up,

    ["K"] = actions.mkdir,
    ["N"] = actions.newfile,
    ["R"] = actions.rename,
    ["Y"] = actions.yank_path,
    ["D"] = actions.delete,
    ["."] = actions.toggle_show_hidden,

    -- mmv
    -- ["M"] = (has_mmv and mmv_actions.mmv) or nil,
  },
})

require("lir.git_status").setup {
  show_ignored = false,
}

vim.api.nvim_set_keymap("n", "-", ":edit %:h<CR>", { noremap = true })
