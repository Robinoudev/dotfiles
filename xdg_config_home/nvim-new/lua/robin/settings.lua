local root = vim.env.USER == 'root'
local home = vim.env.HOME
local config = home .. "/.config/nvim"

-- [[ GLOBALS ]]
vim.o.hlsearch         = true -- Set highlight on search
vim.wo.number          = true -- Make line numbers default
vim.wo.relativenumber  = true -- Make line numbers default
vim.o.mouse            = 'a' -- Enable mouse mode
vim.o.breakindent      = true -- Enable break indent
vim.o.undofile         = true -- Save undo history
vim.wo.signcolumn      = 'yes' -- Turn on signcolumn
vim.o.updatetime       = 250 -- Decrease update time
vim.opt.splitbelow     = true -- split new files under cursor
vim.opt.splitright     = true -- split new files to the right of cursor
vim.opt.scrolloff      = 6 -- minimal no. of screen lines to keep above or under cursor
vim.opt.sidescrolloff  = 3 -- same as scrolloff, but for columns
vim.opt.showbreak      = '↳ ' -- DOWNWARDS ARROW WITH TIP RIGHTWARDS (U+21B3, UTF-8: E2 86 B3)
vim.o.completeopt      = 'menuone,noselect'
vim.opt.clipboard      = "unnamedplus" -- Add the system clipboard to vim
vim.opt.showmode       = false -- don't show mode; --INSERT-- etc. at bottom of statusline
vim.opt.lazyredraw     = true -- don't bother updating screen during macro playback
vim.opt.inccommand     = "split" -- show preview of in preview window (eg. %s/../../g)
vim.opt.cmdheight      = 1
vim.opt.spellcapcheck  = "" -- don't check for capital letters at start of sentence
vim.opt.synmaxcol      = 200 -- don't bother syntax highlighting long lines
vim.opt.pumblend       = 25 -- give the popup window transparency
vim.opt.textwidth      = 80 -- maximum width of text in insert mode for comments
vim.opt.showcmd        = false -- don't show extra info at end of command line
vim.opt.smarttab       = true -- <tab>/<BS> indent/dedent in leading whitespace
vim.opt.ignorecase     = true -- ignore case of normal letters
vim.opt.smartcase      = true -- only ignore the above when pattern has lower case letters only
vim.opt.incsearch      = true -- when typing a search, show where the pattern matches
vim.opt.errorbells     = false -- no error bells when hitting esc in normal mode etc.
vim.opt.joinspaces     = false -- don't autoinsert two spaces after '.', '?', '!' for join command
vim.opt.clipboard      = "unnamedplus" -- Add the system clipboard to vim
vim.opt.guicursor      = "" -- I want an old fashioned blocky cursor
vim.opt.virtualedit    = 'block' -- allow cursor to move where there is no text in visual block mode
vim.opt.writebackup    = false -- don't write backup files
vim.opt.swapfile       = false -- don't create swap files

-- [[ NETRW ]]
vim.g.netrw_browse_split = 0
vim.g.netrw_banner = 0
vim.g.netrw_winsize = 25

-- [[ Highlight on yank ]]
-- See `:help vim.highlight.on_yank()`
local highlight_group = vim.api.nvim_create_augroup('YankHighlight', { clear = true })
vim.api.nvim_create_autocmd('TextYankPost', {
  callback = function()
    vim.highlight.on_yank()
  end,
  group = highlight_group,
  pattern = '*',
})


-- [[ Directory related stuff ]]
vim.opt.directory  = config .. "/nvim/swap//" -- keep swap files out of the way
vim.opt.directory  = vim.opt.directory + "." -- fallback
vim.opt.backupdir  = config .. '/backup//' -- keep backup files out of the way (ie. if 'backup' is ever set)
vim.opt.backupdir  = vim.opt.backupdir + '.' -- fallback
vim.opt.backupskip = vim.opt.backupskip + '*.re,*.rei' -- prevent bsb's watch mode from getting confused (if 'backup' is ever set)

if root then
  vim.opt.undofile = false -- don't create root-owned files
else
  vim.opt.undodir  = config .. '/undo//' -- keep undo files out of the way
  vim.opt.undodir  = vim.opt.undodir + '.' -- fallback
  vim.opt.undofile = true -- actually use undo files
end

if root then
  vim.opt.shada     = "" -- Don't create root-owned files.
  vim.opt.shadafile = "NONE"
else
  -- Defaults:
  --   Neovim: !,'100,<50,s10,h
  --
  -- - ! save/restore global variables (only all-uppercase variables)
  -- - '100 save/restore marks from last 100 files
  -- - <50 save/restore 50 lines from each register
  -- - s10 max item size 10KB
  -- - h do not save/restore 'hlsearch' setting
  --
  -- Our overrides:
  -- - '0 store marks for 0 files
  -- - <0 don't save registers
  -- - f0 don't store file marks
  -- - n: store in ~/.config/nvim/
  --
  vim.opt.shada = "'0,<0,f0,n" .. config .. "shada"
end

vim.opt.listchars = {
  nbsp     = '⦸', -- CIRCLED REVERSE SOLIDUS (U+29B8, UTF-8: E2 A6 B8)
  extends  = '»', -- RIGHT-POINTING DOUBLE ANGLE QUOTATION MARK (U+00BB, UTF-8: C2 BB)
  precedes = '«', -- LEFT-POINTING DOUBLE ANGLE QUOTATION MARK (U+00AB, UTF-8: C2 AB)
  tab      = '▷⋯', -- WHITE RIGHT-POINTING TRIANGLE (U+25B7, UTF-8: E2 96 B7) + MIDLINE HORIZONTAL ELLIPSIS (U+22EF, UTF-8: E2 8B AF)
  trail    = '•', -- BULLET (U+2022, UTF-8: E2 80 A2)
}
vim.opt.fillchars = {
  diff = '∙', -- BULLET OPERATOR (U+2219, UTF-8: E2 88 99)
  eob  = ' ', -- NO-BREAK SPACE (U+00A0, UTF-8: C2 A0) to suppress ~ at EndOfBuffer
  fold = '·', -- MIDDLE DOT (U+00B7, UTF-8: C2 B7)
  vert = '┃', -- BOX DRAWINGS HEAVY VERTICAL (U+2503, UTF-8: E2 94 83)
}
