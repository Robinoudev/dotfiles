vim.g.mapleader = " "
vim.keymap.set("n", "<leader>pv", vim.cmd.Ex)

-- Keymaps for better default experience
-- See `:help vim.keymap.set()`
vim.keymap.set({ 'n', 'v' }, '<Space>', '<Nop>', { silent = true })

-- Remap for dealing with word wrap
vim.keymap.set('n', 'k', "v:count == 0 ? 'gk' : 'k'", { expr = true, silent = true })
vim.keymap.set('n', 'j', "v:count == 0 ? 'gj' : 'j'", { expr = true, silent = true })

-- Center screen after half page scrolls
vim.keymap.set("n", "<C-d>", "<C-d>zz")
vim.keymap.set("n", "<C-u>", "<C-u>zz")

-- Diagnostic keymaps
vim.keymap.set('n', '[d', vim.diagnostic.goto_prev)
vim.keymap.set('n', ']d', vim.diagnostic.goto_next)
vim.keymap.set('n', '<leader>e', vim.diagnostic.open_float)
vim.keymap.set('n', '<leader>q', vim.diagnostic.setloclist)

-- undo tree
vim.keymap.set("n", "<leader>u", vim.cmd.UndotreeToggle)

-- Git
vim.keymap.set("n", "<leader>gs", vim.cmd.Git, { silent = true })
vim.keymap.set("n", "<leader>gb", vim.cmd.GBrowse, { silent = true })
vim.keymap.set("n", "<leader>gh", ":diffget //3<CR>")
vim.keymap.set("n", "<leader>gu", ":diffget //2<CR>")

vim.keymap.set("n", "<leader>bd", vim.cmd.bd, { silent = true })
vim.keymap.set("n", "<leader>q", vim.cmd.q, { silent = true })

-- turn on very magic
vim.keymap.set("n", "/", "/\\v")
vim.keymap.set("n", "?", "?\\v")
vim.keymap.set("v", "/", "/\\v")
vim.keymap.set("v", "?", "?\\v")

vim.keymap.set("n", "<esc>", vim.cmd.noh, { silent = true })

-- Tabs
vim.keymap.set("n", "<Left>", vim.cmd.tabprevious, { silent = true })
vim.keymap.set("n", "<Right>", vim.cmd.tabnext, { silent = true })


-- toggle folds w/ shift tab
vim.keymap.set("n", ",", "za")

-- Indent selected lines and keep selection
vim.keymap.set("v", "<", "<gv")
vim.keymap.set("v", ">", ">gv")

-- quicfix list scrolling
vim.keymap.set("n", "]q", vim.cmd.cnext, { silent = true })
vim.keymap.set("n", "[q", vim.cmd.cprevious, { silent = true })

-- move selected lines up or down
vim.keymap.set("v", "J", ":m '>+1<CR>gv=gv")
vim.keymap.set("v", "K", ":m '<-2<CR>gv=gv")
