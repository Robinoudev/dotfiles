require("robin.packer")
require("robin.remap")
require("robin.settings")

function R(name)
    require("plenary.reload").reload_module(name)
end

-- auto remove white space
local augroup = vim.api.nvim_create_augroup
local MyGroup = augroup('MyGroup', {})
local autocmd = vim.api.nvim_create_autocmd
autocmd({"BufWritePre"}, {
    group = MyGroup,
    pattern = "*",
    command = "%s/\\s\\+$//e",
})
