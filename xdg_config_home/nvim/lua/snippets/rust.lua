local ls = require('luasnip')
local s = ls.snippet
local sn = ls.snippet_node
local t = ls.text_node
local i = ls.insert_node
local f = ls.function_node
local c = ls.choice_node
local d = ls.dynamic_node
local r = ls.restore_node

local fmt = require("luasnip.extras.fmt").fmt

ls.add_snippets("rust", {
  s("f", fmt("fn {}({}){} {{\n\t{}\n}}", { i(1, "name"), i(2), i(3), i(0) })),
  s("tn", fmt("fn new({}) -> Self {{\n\t{}\n\n\tSelf {{ {} }}\n}}", { i(1, "args"), i(2), i(0) })),
  s(
    "modtest",
    fmt(
      [[
        #[cfg(test)]
        mod test {{
          {}

            {}
        }}
      ]],
      {
        c(1, { t "  use super::*;", t "" }),
        i(0),
      }
    )
  )
})
