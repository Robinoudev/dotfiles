local ls = require('luasnip')
local s = ls.snippet
local sn = ls.snippet_node
local t = ls.text_node
local i = ls.insert_node
local f = ls.function_node
local c = ls.choice_node
local d = ls.dynamic_node
local r = ls.restore_node

local fmt = require("luasnip.extras.fmt").fmt

-- TODO(robin): find a way to make this func available in all other snippet
-- files as well
-- Helper function to insert same text in multiple places
local same = function(index)
  return f(function(arg)
    return arg[1]
  end, { index })
end

ls.add_snippets("lua", {
  s("lv", fmt("local {} = {}", { i(1, "name"), i(0) })),
  s("f", fmt("function({})\n\t{}\nend{}", { i(1), i(2), i(0) })),
  s("lf", fmt("local function{}({})\n\t{}\nend{}", { i(1, "name"), i(2), i(3), i(0) })),
  s(
    "req",
    fmt(
      [[
        local {} = require "{}"
      ]],
      {
        f(function(import_name)
          local parts = vim.split(import_name[1][1], ".", true)
          return parts[#parts] or ""
        end, { 1 }),
        i(1)
      }
    )
  )
})
