local ls = require('luasnip')
local s = ls.snippet
local sn = ls.snippet_node
local t = ls.text_node
local i = ls.insert_node
local f = ls.function_node
local c = ls.choice_node
local d = ls.dynamic_node
local r = ls.restore_node

local fmt = require("luasnip.extras.fmt").fmt

local same = function(index)
  return f(function(args)
    return args[1]
  end, { index })
end


ls.add_snippets("dart", {
  s(
    { trig = "class", dscr = "Create new class" },
    {
    t({ "class " }),
    i(1, "MyClass"),
    t({ " extends " }),
    i(2, "StatefulWidget"),
    t({ " {", "" }),
    i(0, "  // TODO: implement"),
    t({ "", "" }),
    t("}")
  }
  ),
  s(
    "eventclass",
    fmt(
      [[
        class {} extends {} {{
          const {};
        }}
      ]],
      {
      i(1),
      i(2),
      same(3)
    }
    )
  )
})
