local ls = require('luasnip')
local s = ls.snippet
local sn = ls.snippet_node
local t = ls.text_node
local i = ls.insert_node
local f = ls.function_node
local c = ls.choice_node
local d = ls.dynamic_node
local r = ls.restore_node

local fmt = require("luasnip.extras.fmt").fmt

local same = function(index)
  return f(function(args)
    return args[1]
  end, { index })
end

ls.add_snippets("nix", {
  s(
    "mod",
    fmt(
      [[
        {{ config, lib, pkgs, inputs, ... }}:

        with lib;
        with lib.my;
        let cfg = config.modules.{}.{};
        in {{
          options.modules.{}.{} = {{
            enable = mkBoolOpt false;
          }};

          config = mkIf cfg.enable {{

          }};
        }}
      ]],
      {
      i(1),
      i(2),
      same(1),
      same(2),
    }
    )
  )
})
