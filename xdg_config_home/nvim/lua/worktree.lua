local Worktree = require("git-worktree")
local Job = require("plenary.job")

local function is_spotta()
  return vim.env.SPOTTA == 1
end

-- TODO: fix this
Worktree.on_tree_change(function(op, path, upstream)
  if op == "create" and is_spotta() then
    Job:new({
      "flutter", "pub", "get"
    })
  end
end)
