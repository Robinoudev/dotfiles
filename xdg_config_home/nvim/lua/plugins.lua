-- packer install path
local install_path = vim.fn.stdpath 'data' .. '/site/pack/packer/start/packer.nvim'

-- clone packer when it is not yet installed
if vim.fn.empty(vim.fn.glob(install_path)) > 0 then
  vim.fn.execute('!git clone https://github.com/wbthomason/packer.nvim ' .. install_path)
end

vim.cmd [[ packadd packer.nvim ]]

return require('packer').startup {
  function(use)
    -- Packer can manage itself
    use 'wbthomason/packer.nvim'

    -- Local plugins
    use '~/code/personal/timer'

    -- FZF
    use 'junegunn/fzf'
    use 'junegunn/fzf.vim'

    use 'tpope/vim-fugitive'
    use 'justinmk/vim-dirvish'

    use 'numToStr/Comment.nvim'
    use 'JoosepAlviste/nvim-ts-context-commentstring'

    -- Neovim Tree shitter
    use {
      "nvim-treesitter/nvim-treesitter",
      run = ":TSUpdate",
    }
    use 'nvim-treesitter/playground'
    use 'nvim-treesitter/nvim-treesitter-context'

    -- colors
    use 'norcalli/nvim-base16.lua'
    use "EdenEast/nightfox.nvim"
    use 'tjdevries/colorbuddy.vim'
    use 'tjdevries/gruvbuddy.nvim'
    use 'folke/tokyonight.nvim'
    use {
      'mcchrish/zenbones.nvim',
      requires = "rktjmp/lush.nvim"
    }
    use("gruvbox-community/gruvbox")
    use({ "catppuccin/nvim", as = "catppuccin" })
    use({ 'rose-pine/neovim', as = 'rose-pine' })

    use({
      'wincent/command-t',
      config = function()
        require('wincent.commandt').setup()
      end
    })
    use 'wincent/pinnacle'

    -- Telescope
    use 'nvim-lua/plenary.nvim'
    use {
      'nvim-telescope/telescope.nvim',
      requires = { { 'nvim-lua/plenary.nvim' } }
    }
    use { 'nvim-telescope/telescope-fzf-native.nvim', run = 'make' }
    use 'nvim-telescope/telescope-ui-select.nvim'
    use "nvim-telescope/telescope-frecency.nvim"
    use "nvim-telescope/telescope-smart-history.nvim"
    use "tami5/sqlite.lua"

    use 'ThePrimeagen/git-worktree.nvim'

    -- Dadbod for databases
    use('tpope/vim-dadbod')
    use('kristijanhusak/vim-dadbod-ui')

    -- LSP
    use 'neovim/nvim-lspconfig'
    use 'hrsh7th/cmp-nvim-lsp'
    use 'hrsh7th/cmp-nvim-lua'
    use 'hrsh7th/cmp-path'
    use 'hrsh7th/cmp-buffer'
    use 'hrsh7th/nvim-cmp'
    use 'tjdevries/complextras.nvim'
    use 'onsails/lspkind-nvim'
    use 'nvim-lua/lsp_extensions.nvim'

    -- rust
    use 'rust-lang/rust.vim'
    use 'togglebyte/togglerust'
    use 'simrat39/rust-tools.nvim'

    -- flutter
    use {
      'akinsho/flutter-tools.nvim',
      requires = 'nvim-lua/plenary.nvim',
      config = function()
        require "flutter-tools".setup {}
      end
    }

    -- debugger
    use "mfussenegger/nvim-dap"
    use "rcarriga/nvim-dap-ui"
    use 'Pocco81/DAPInstall.nvim'

    -- basic utils
    use 'windwp/nvim-autopairs'
    use 'wincent/loupe'
    use 'tpope/vim-surround'
    use 'L3MON4D3/LuaSnip'
    use 'saadparwaiz1/cmp_luasnip'
    use 'molleweide/LuaSnip-snippets.nvim'
    use {
      'pwntester/octo.nvim',
      requires = {
        'nvim-lua/plenary.nvim',
        'nvim-telescope/telescope.nvim',
        'kyazdani42/nvim-web-devicons',
      },
      config = function()
        require "octo".setup()
      end
    }
    use 'ThePrimeagen/harpoon'
    use 'rcarriga/nvim-notify'
    use 'tpope/vim-abolish'
    use({
      'crispgm/nvim-tabline',
      config = function()
        require('tabline').setup({})
      end,
    })
    use 'folke/twilight.nvim'
    use 'folke/zen-mode.nvim'
    use { 'sindrets/diffview.nvim', requires = 'nvim-lua/plenary.nvim' }
    use 'sar/neogit.nvim'
    use 'tjdevries/cyclist.vim'

    -- tmux
    use 'christoomey/vim-tmux-navigator'

    -- notes
    use 'renerocksai/telekasten.nvim'
    use 'renerocksai/calendar-vim'
    use 'iamcco/markdown-preview.nvim'
    use 'vim-markdown-toc'
  end
}
