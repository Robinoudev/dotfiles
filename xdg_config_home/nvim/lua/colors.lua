vim.opt.termguicolors = true
-- require('colorbuddy').colorscheme('gruvbuddy')
vim.cmd("colorscheme rose-pine")

-- remove thick split borders
vim.cmd([[ highlight WinSeparator guibg=None ]])

vim.cmd([[
  highlight linenr term=bold cterm=NONE ctermfg=darkgrey ctermbg=NONE
  highlight! link signcolumn linenr
  highlight colorcolumn ctermbg=darkgrey
  highlight NormalFloat ctermbg=NONE ctermfg=NONE
]])
