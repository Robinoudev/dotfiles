-- Install packer when not installed
-- stdpath('data') equals to $HOME/.local/share/nvim
local packer_path = vim.fn.stdpath('data') .. '/site/pack/packer/start/packer.nvim'
if vim.fn.empty(vim.fn.glob(packer_path)) > 0 then
  vim.fn.system({ 'git', 'clone', 'https://github.com/wbthomason/packer.nvim', packer_path })
  vim.api.nvim_command('source $HOME/.config/nvim/init.lua')
  vim.api.nvim_command('PackerSync')
end

-- Load all modules in lua folder
local modules = {
  'status',
  'plugins',
  'colors',
  'worktree'
}

for i, a in ipairs(modules) do
  local ok, err = pcall(require, a)
  if not ok then
    error("Error calling " .. a .. err)
  end
end

-- auto commands
vim.cmd [[
filetype indent plugin on
syntax on

lua require'nvim-treesitter.configs'.setup { highlight = { enable = true }, context_commentstring = { enable = true } }

if executable('rg')
    let g:rg_derive_root='true'
    set grepprg=rg\ --vimgrep
endif

let loaded_matchparen = 1

augroup highlight_yank
    autocmd!
    autocmd TextYankPost * silent! lua require'vim.highlight'.on_yank({timeout = 40})
augroup END

" JUST WRITE
com! W w

fun! TrimWhitespace()
    let l:save = winsaveview()
    keeppatterns %s/\s\+$//e
    call winrestview(l:save)
endfun

augroup TRIM_WHITESPACE
    autocmd!
    autocmd BufWritePre * :call TrimWhitespace()
    autocmd BufEnter,BufWinEnter,TabEnter *.rs :lua require'lsp_extensions'.inlay_hints{}
augroup END

" Some terminal defaults
au TermOpen term://* setlocal nonumber norelativenumber signcolumn=no | setfiletype terminal

augroup open-tabs
    au!
    au VimEnter * ++nested if !&diff | tab all | tabfirst | endif
augroup end
]]

-- vim: foldmethod=marker
