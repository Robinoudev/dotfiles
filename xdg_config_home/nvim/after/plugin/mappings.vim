" NORMAL MODE {{{
nnoremap <silent><leader>bd :bd<CR>
nnoremap <leader>q :q<CR>

" clear search highlight with escape
nnoremap <silent> <esc> :noh<cr>

" Turn on 'verymagic' mode when searching
" nnoremap / /\v
" nnoremap ? ?\v

" Tabs
nnoremap <silent><Left> :tabprevious<CR>
nnoremap <silent><Right> :tabnext<CR>

" Folds and splits
let g:tmux_navigator_no_mappings = 1
nnoremap <silent> <C-h> :TmuxNavigateLeft<CR>
nnoremap <silent> <C-l> :TmuxNavigateRight<CR>
nnoremap <silent> <C-j> :TmuxNavigateDown<CR>
nnoremap <silent> <C-k> :TmuxNavigateUp<CR>

" toggle folds w/ shift tab
nnoremap , za

" Yank from cursor to end of line
nnoremap Y y$

" FZF
nnoremap <C-p> :Telescope find_files<CR>
nnoremap <leader><leader> :Telescope find_files<CR>
nnoremap <leader>pf :Files<CR>
nnoremap <leader>ps :Telescope live_grep<CR>
nnoremap <leader>' :Telescope resume<CR>
nnoremap <leader>pw :Telescope grep_string<CR>
nnoremap <leader>pb :Telescope buffers<CR>
nnoremap <leader>pt :Rg TODO\(robin\)<CR>
nnoremap <leader>cd :Telescope lsp_definitions<CR>
nnoremap <leader>cs :Telescope lsp_document_symbols<CR>
nnoremap <leader>ht :Telescope help_tags<CR>
nnoremap <leader>fr :Telescope frecency<CR>

" LSP
nnoremap <silent><leader>cr :lua vim.lsp.buf.rename()<CR>
nnoremap <silent><leader>ca :lua vim.lsp.buf.code_action()<CR>
nnoremap <silent><leader>vd :lua vim.diagnostic.open_float()<CR>
nnoremap <silent>]d :lua vim.diagnostic.goto_next()<CR>
nnoremap <silent>[d :lua vim.diagnostic.goto_prev()<CR>

" Worktrees
nnoremap <leader>wc :lua require('telescope').extensions.git_worktree.create_git_worktree()<CR>
nnoremap <leader>ws :lua require('telescope').extensions.git_worktree.git_worktrees()<CR>

" Git
nmap <silent><leader>gs :G<CR>
nmap <leader>gp :Git push<Space>
nmap <leader>gh :diffget //3<CR>
nmap <leader>gu :diffget //2<CR>
nmap <leader>gc :GBranches<CR>
nmap <leader>gfa :Git fetch --all --prune<CR>
" }}}

" VISUAL MODE {{{
" Delete selected text without overwriting register
vnoremap X "_d

" paste over selected text without overwriting register
vnoremap <leader>p "_dP

" Indent selected lines and keep selection
vmap < <gv
vmap > >gv

" Also turn on 'verymagic' mode on visual select search
vnoremap / /\v
vnoremap ? ?\v
" }}}

" COMMAND MODE {{{
" Yank the current file to the system clipboard
cnoremap yf!! !echo % <bar> xclip -selection clipboard

" Save file as sudo on files that require root permission
cnoremap w!! execute 'silent! write !sudo tee % >/dev/null' <bar> edit!

" Harpooning
nnoremap <leader>ha :lua require("harpoon.mark").add_file()<CR>
nnoremap <silent><leader>ho :lua require("harpoon.ui").toggle_quick_menu()<CR>
nnoremap <silent><leader>+ :lua require("harpoon.ui").nav_file(1)<CR>
nnoremap <silent><leader>[ :lua require("harpoon.ui").nav_file(2)<CR>
nnoremap <silent><leader>{ :lua require("harpoon.ui").nav_file(3)<CR>
nnoremap <silent><leader>( :lua require("harpoon.ui").nav_file(4)<CR>
" }}}
