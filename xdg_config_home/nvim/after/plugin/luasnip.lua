-- Sanity check if we have luasnip
if vim.g.snippets ~= "luasnip" or not pcall(require, "luasnip") then
  return
end

local ls = require "luasnip"
local snippet = ls.s
local events = require "luasnip.util.events"
local types = require "luasnip.util.types"
local t = ls.text_node
local c = ls.choice_node
-- Function Node
--  Takes a function that returns text
local f = ls.function_node

ls.config.set_config {
  -- Keep around last snippet to jump back whenever
  history = true,

  -- This makes it possible to update the text in two places of a snippet at the
  -- same time
  updateevents = "TextChanged,TextChangedI",

  -- Some information when using a snippet that has choices
  ext_opts = {
    [types.choiceNode] = {
      active = {
        virt_text = { { " <- Current Choice", "NonTest" } },
      },
    },
  },
}

ls.add_snippets(nil, {
  all = {
    snippet({ trig = "date", describe = "insert current date" }, {
      f(function()
        return string.format(string.gsub(vim.bo.commentstring, "%%s", " %%s"), os.date())
      end, {}),
    }),
    snippet("todo", {
      c(1, {
        f(function()
          return string.format(string.gsub(vim.bo.commentstring, "%%s", " %%s"), "TODO(robin): ")
        end, {}),
        f(function()
          return string.format(string.gsub(vim.bo.commentstring, "%%s", " %%s"), "NOTE(robin): ")
        end, {}),
        f(function()
          return string.format(string.gsub(vim.bo.commentstring, "%%s", " %%s"), "FIXME(robin): ")
        end, {}),
      }),
    }),
  }
})

-- Setup C-k to expand current item or jump to next item
vim.keymap.set({ "i", "s" }, "<c-k>", function()
  if ls.expand_or_jumpable() then
    ls.expand_or_jump()
  end
end, { silent = true })

-- Setup C-j to jump backwards
vim.keymap.set({ "i", "s" }, "<c-j>", function()
  if ls.jumpable(-1) then
    ls.jump(-1)
  end
end, { silent = true })

-- Setup C-l to select within a list of options
-- <c-l> is selecting within a list of options.
-- This is useful for choice nodes (introduced in the forthcoming episode 2)
vim.keymap.set("i", "<c-l>", function()
  if ls.choice_active() then
    ls.change_choice(1)
  end
end)

-- Setup C-i to actually confirm the selected choice
vim.keymap.set("i", "<c-u>", require "luasnip.extras.select_choice")

-- Finally, load all custom snippet files
for _, ft_path in ipairs(vim.api.nvim_get_runtime_file("lua/snippets/*.lua", true)) do
  loadfile(ft_path)()
end
