-- Setup auto formatters
local format_group = vim.api.nvim_create_augroup("LspFormat", { clear = true })
vim.api.nvim_create_autocmd(
  "BufWritePre",
  {
    command = "lua vim.lsp.buf.format()",
    group = format_group,
    pattern = { "*.lua", "*.dart", "*.rs" },
  }
)


local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities.textDocument.completion.completionItem.snippetSupport = true
capabilities = require('cmp_nvim_lsp').default_capabilities(capabilities)

-- Setup lspconfig.
require('lspconfig').tsserver.setup {
  capabilities = capabilities,
}
require('lspconfig').solargraph.setup {
  capabilities = capabilities,
  settings = { solargraph = { diagnostics = true; formatting = true; logLevel = 'debug'; } }
}
require('lspconfig').elixirls.setup {
  capabilities = capabilities,
  cmd = { "elixir-ls" };
}
require('lspconfig').rust_analyzer.setup {
  capabilities = require('cmp_nvim_lsp').default_capabilities(vim.lsp.protocol.make_client_capabilities()),
  cmd = { "rustup", "run", "nightly", "rust-analyzer" },
}

local runtime_path = vim.split(package.path, ';')
table.insert(runtime_path, "lua/?.lua")
table.insert(runtime_path, "lua/?/init.lua")
require('lspconfig').sumneko_lua.setup {
  capabilities = capabilities,
  settings = {
    Lua = {
      runtime = {
        -- Tell the language server which version of Lua you're using (most likely LuaJIT in the case of Neovim)
        version = 'LuaJIT',
        -- Setup your lua path
        path = runtime_path,
      },
      diagnostics = {
        -- Get the language server to recognize the `vim` global
        globals = { 'vim' },
      },
      workspace = {
        -- Make the server aware of Neovim runtime files
        library = vim.api.nvim_get_runtime_file("", true),
      },
      -- Do not send telemetry data containing a randomized but unique identifier
      telemetry = {
        enable = false,
      },
    },
  },
}
require('lspconfig').dartls.setup {
  init_options = {
    closingLabels = true,
    outline = true,
  },
  callbacks = {
    -- get_callback can be called with or without arguments
    ['dart/textDocument/publishClosingLabels'] = require('lsp_extensions.dart.closing_labels').get_callback({ highlight = "Special", prefix = " >> " }),
  },
  capabilities = capabilities
}
