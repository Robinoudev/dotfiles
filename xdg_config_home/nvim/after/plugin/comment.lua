if not pcall(require, "Comment") then
  return
end

require("Comment").setup {
  opleader = {
    line = "gc",
    block = "gb",
  },
  mappings = {
    basic = true,
    extra = true,
  },
  toggler = {
    line = "gcc",
    block = "gcb",
  },
}

local comment_ft = require "Comment.ft"
comment_ft.set("lua", { "--%s", "--[[%s]]" })
comment_ft.set("dart", { "//%s", "//[[%s]]" })
