" -----------------------------------------------------------------------------
"     - General settings -
" -----------------------------------------------------------------------------
set textwidth=99
set shiftwidth=4
set tabstop=4

" -----------------------------------------------------------------------------
"     - Key mappings -
" -----------------------------------------------------------------------------
nmap <C-b> :Compile<CR>
nmap <leader>x :Cargo run<CR>
nmap <leader>f :RustFmt<CR>
