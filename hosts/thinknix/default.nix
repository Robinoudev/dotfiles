{ config, pkgs, ...}:

{
  imports =
    [(import ./hardware-configuration.nix)];

  networking.hostName = "thinknix"; # Define your hostname.

  # intel accelerated video playback
  nixpkgs.config.packageOverrides = pkgs: {
    vaapiIntel = pkgs.vaapiIntel.override { enableHybridCodec = true; };
  };
  hardware.opengl = {
    enable = true;
    extraPackages = with pkgs; [
      intel-media-driver # LIBVA_DRIVER_NAME=iHD
      vaapiIntel         # LIBVA_DRIVER_NAME=i965 (older but works better for Firefox/Chromium)
      vaapiVdpau
      libvdpau-va-gl
    ];
  };

  services.xserver = {
    libinput.enable = true;
    dpi = 122;
  };
  services.mullvad-vpn.enable = true;
}
