{ config, pkgs, lib, ...}:

with lib;
with lib.my;
{
  imports =
    [
      ../home.nix
      ./hardware-configuration.nix
    ];

  modules = {
    desktop = {
      herbstluftwm.enable = true;
      brave.enable = true;
    };
    editors = {
      nvim.enable = true;
      emacs.enable = true;
    };
    programs = {
      kitty.enable = true;
      alacritty.enable = true;
      firefox.enable = true;
    };
    shell = {
      zsh.enable = true;
      tmux.enable = true;
      git.enable = true;
    };
    development = {
      typescript.enable = true;
    };
    services = {
      docker.enable = true;
      postgresql.enable = true;
    };
  };
  networking.hostName = "gamenix"; # Define your hostname.

  hardware.opengl = {
    enable = true;
    driSupport = true;
    driSupport32Bit = true;
  };

  services.xserver = {
    videoDrivers = [ "nvidia" ];
    dpi = 144;
  };

  # some work related stuff
  networking.extraHosts =
    ''
    127.0.0.1 omdenken.craft.local
    127.0.0.1 herokuPostgresql
    127.0.0.1 herokuRedis
    '';

  # I need flatpaks for work unfortunately
  xdg.portal = {
    enable = true;
    extraPortals = [ pkgs.xdg-desktop-portal-gtk ];
  };
  services.flatpak.enable = true;
}
