{ config, options, pkgs, lib, ... }:

with lib;
with lib.my;
let cfg = config.modules.desktop.herbstluftwm;
in {
  options.modules.desktop.herbstluftwm = {
    enable = mkBoolOpt false;
  };
  config = mkIf cfg.enable {
    user.packages = with pkgs; [
      rofi
      lxsession
    ];

    # FIXME(robin): for some reason herbstluftwm does not load config file
    home.configFile = {
      "herbstluftwm" = {
        source = ../../xdg_config_home/herbstluftwm;
        recursive = true;
      };
      "rofi" = {
        source = ../../xdg_config_home/rofi;
        recursive = true;
      };
    };
    services.xserver = {
      enable = true;
      layout = "us";
      # xkbOptions = "caps:escape";
      displayManager = {
        lightdm.enable = true;
        defaultSession = "none+herbstluftwm";
      };
      windowManager.herbstluftwm = {
        enable = true;
        configFile = ../../xdg_config_home/herbstluftwm/autostart;
      };
    };
  };
}
