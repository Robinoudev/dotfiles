{ config, options, pkgs, lib, ... }:

with lib;
with lib.my;
let cfg = config.modules.desktop.brave;
in {
  options.modules.desktop.brave = {
    enable = mkBoolOpt false;
  };

  config = {
    user.packages = with pkgs; [brave];
  };
}
