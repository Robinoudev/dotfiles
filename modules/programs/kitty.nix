{ config, pkgs, user, options, lib, ... }:

with lib;
with lib.my;
let cfg = config.modules.programs.kitty;
in {
  options.modules.programs.kitty = {
    enable = mkBoolOpt false;
  };
  config = mkIf cfg.enable {
    user.packages = with pkgs; [kitty];
    home = {
      configFile = {
        "kitty" = {
          source = ../../xdg_config_home/kitty;
          recursive = true;
        };
      };
    };
  };
}
