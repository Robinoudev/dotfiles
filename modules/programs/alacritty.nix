{ config, pkgs, user, options, lib, ... }:

with lib;
with lib.my;
let cfg = config.modules.programs.alacritty;
in {
  options.modules.programs.alacritty = {
    enable = mkBoolOpt false;
  };
  config = mkIf cfg.enable {
    user.packages = with pkgs; [alacritty];
    home = {
      configFile = {
        "alacritty" = {
          source = ../../xdg_config_home/alacritty;
          recursive = true;
        };
      };
    };
  };
}

