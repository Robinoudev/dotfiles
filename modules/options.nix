# This modularizes the config and will allow to add configurable options in the
# config
{ config, options, lib, home-manager, ... }:

with lib;
with lib.my;
{
  options = with types; {
    user = mkOpt attrs {};

    # determine where on the system the nixos config resides
    dotfiles = {
      dir = mkOpt path
        (removePrefix "/mnt"
          (findFirst pathExists (toString ../.) [
            "/mnt/etc/dotfiles"
            "/etc/dotfiles"
            "${config.user.home}/code/dotfiles"
          ]));
      binDir = mkOpt path "${config.dotfiles.dir}/bin";
      configDir = mkOpt path "${config.dotfiles.dir}/xdg_config_home";
      modulesDir = mkOpt path "${config.dotfiles.dir}/modules";
    };

    home = {
      file = mkOpt' attrs {} "Files in $HOME";
      configFile = mkOpt' attrs {} "Files in $XDG_CONFIG_HOME";
      dataFile = mkOpt' attrs {} "Files in $XDG_DATA_HOME";
    };
  };

  config = {
    user =
      let user = builtins.getEnv "USER";
          name = if elem user [ "" "root" ] then "robin" else user;
      in {
        inherit name;
        description = "Primary user account";
        extraGroups = [ "wheel" ];
        isNormalUser = true;
        home = "/home/${name}";
        group = "users";
        uid = 1000;
      };

      # setup home-manager for dotfiles management
      home-manager = {
        useUserPackages = true;
        users.${config.user.name} = {
          home = {
            file = mkAliasDefinitions options.home.file;
            stateVersion = config.system.stateVersion;
          };
          xdg = {
            configFile = mkAliasDefinitions options.home.configFile;
            dataFile = mkAliasDefinitions options.home.dataFile;
          };
        };
      };

      users.users.${config.user.name} = mkAliasDefinitions options.user;

      nix = let users = [ "root" config.user.name ]; in {
        trustedUsers = users;
        allowedUsers = users;
      };
  };
}
