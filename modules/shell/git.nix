{ options, config, pkgs, lib, ... }:

with lib;
with lib.my;
let cfg = config.modules.shell.git;
in {
  options.modules.shell.git = {
    enable = mkBoolOpt false;
  };

  config = mkIf cfg.enable {
    programs.git.enable = true;

    user.packages = with pkgs; [
      gh
    ];

    home.configFile = {
      "git" = {
        source = ../../xdg_config_home/git;
        recursive = true;
      };
    };
  };
}

