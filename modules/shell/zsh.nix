{ options, config, pkgs, lib, ... }:

with lib;
with lib.my;
let cfg = config.modules.shell.zsh;
in {
  options.modules.shell.zsh = {
    enable = mkBoolOpt false;
  };

  config = mkIf cfg.enable {
    users.defaultUserShell = pkgs.zsh;
    programs.zsh.enable = true;

    user.packages = with pkgs; [
        direnv
        starship
        ripgrep
        exa
        fd
        fzf
        skim
    ];

    home.configFile = {
      "zsh" = {
        source = ../../xdg_config_home/zsh;
        recursive = true;
      };
      ".zshenv".source = ../../xdg_config_home/dot-zshenv;
      "starship.toml".source = ../../xdg_config_home/starship.toml;
    };
  };
}
