{ options, config, pkgs, lib, ... }:

with lib;
with lib.my;
let cfg = config.modules.shell.tmux;
in {
  options.modules.shell.tmux = {
    enable = mkBoolOpt false;
  };

  config = mkIf cfg.enable {
    user.packages = with pkgs; [ tmux ];

    home.configFile = {
      "tmux" = {
        source = ../../xdg_config_home/tmux;
        recursive = true;
      };
    };
  };
}
