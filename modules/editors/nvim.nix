{ config, lib, pkgs, inputs, ... }:

with lib;
with lib.my;
let cfg = config.modules.editors.nvim;
in {
  options.modules.editors.nvim = {
    enable = mkBoolOpt false;
  };

  config = mkIf cfg.enable {
    nixpkgs.overlays = [ inputs.neovim-nightly-overlay.overlay ];
    user.packages = with pkgs; [
      gh
      tree-sitter
      neovim-nightly
      sqlite
      xclip           # for nvim clipboard access
    ];
    home.configFile = {
      "nvim" = {
        source = ../../xdg_config_home/nvim;
        recursive = true;
      };
      "nvim/after/plugin/sqlite.lua".text = ''
        vim.cmd([[let g:sqlite_clib_path = '${pkgs.sqlite.out}/lib/libsqlite3.so']])
      '';
    };
  };
}
