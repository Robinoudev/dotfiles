{ config, lib, pkgs, inputs, ... }:

with lib;
with lib.my;
let cfg = config.modules.editors.emacs;
    configDir = config.dotfiles.configDir;
in {
  options.modules.editors.emacs = {
    enable = mkBoolOpt false;
    doom = rec {
      enable = mkBoolOpt false;
      forgeUrl = mkOpt types.str "https://github.com";
      repoUrl = mkOpt types.str "${forgeUrl}/hlissner/doom-emacs";
    };
  };

  config = mkIf cfg.enable {
    nixpkgs.overlays = [ inputs.emacs-overlay.overlay ];

    user.packages = with pkgs; [
      binutils # this is needed for native-comp
      # 29 + pgtk + native-comp
      ((emacsPackagesFor emacsPgtkGcc).emacsWithPackages (epkgs: [
        epkgs.vterm
      ]))

      # dependencies for doom
      git
      (ripgrep.override {withPCRE2 = true;})
      gnutls
      fd
      zstd
      pinentry_emacs
      imagemagick

      (aspellWithDicts (ds: with ds; [
        en en-computers en-science nl
      ]))
      editorconfig-core-c # per-project style config
      sqlite
      texlive.combined.scheme-medium
    ];

    fonts.fonts = [ pkgs.emacs-all-the-icons-fonts ];

    home.configFile = {
      "doom" = {
        source = ../../xdg_config_home/doom;
        recursive = true;
      };
    };

    system.userActivationScripts = mkIf cfg.doom.enable {
      installDoomEmacs = ''
        if [ ! -d "$XDG_CONFIG_HOME/emacs" ]; then
          git clone --depth=1 --single-branch "${cfg.doom.repoUrl}" "$XDG_CONFIG_HOME/emacs"
        fi
      '';
    };
  };
}
