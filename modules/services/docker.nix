{ pkgs, config, lib, ... }:

with lib;
with lib.my;
let cfg = config.modules.services.docker;
in {
  options.modules.services.docker = {
    enable = mkBoolOpt false;
  };
  config = mkIf cfg.enable {
    user.extraGroups = [ "docker" ];
    virtualisation.docker = {
      enable = true;
    };
  };
}
