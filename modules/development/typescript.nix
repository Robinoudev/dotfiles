{ options, config, pkgs, lib, ... }:

with lib;
with lib.my;
let cfg = config.modules.development.typescript;
in {
  options.modules.development.typescript = {
    enable = mkBoolOpt false;
  };

  config = mkIf cfg.enable {
    user.packages = with pkgs; [
      nodejs_latest
      yarn
      # FIXME(robin): this does not work
      # nodePackages_latest.typescript-language-server
    ];
  };
}
