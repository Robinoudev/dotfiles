{ inputs, config, lib, pkgs, ... }:

with lib;
with lib.my;
{
 imports =
    [ inputs.home-manager.nixosModules.home-manager ]
    ++ (mapModulesRec' (toString ./modules) import);

  boot = {
    kernelPackages = pkgs.linuxPackages_latest;
    loader = {
      efi = {
        canTouchEfiVariables = true;
        efiSysMountPoint = "/boot";
      };
      grub = {
        enable = true;
        version = 2;
        devices = ["nodev"];
        efiSupport = true;
        useOSProber = true;
        configurationLimit = 5;
      };
    };
  };


  hardware.opengl.enable = true;
  # Enable sound.
  sound.enable = true;
  hardware.pulseaudio.enable = true;

  networking.networkmanager.enable = true;  # Easiest to use and most distros use this by default.

  time.timeZone = "Europe/Amsterdam";
  i18n.defaultLocale = "en_US.UTF-8";
   console = {
     font = "Lat2-Terminus16";
     keyMap = "us";
   };

  programs.gnupg.agent = {
    enable = true;
    enableSSHSupport = true;
  };
  services.openssh.enable = true;

  # Packages for the entire system (every user)
   environment.systemPackages = with pkgs; [
     vim
     bind
     cached-nix-shell
     coreutils
     git
     vim
     wget
     unzip
     bc
     gnumake
     gcc
     cmake
     # Mounting different filesystems
     bashmount
     sshfs
     exfat
     ntfs3g
     hfsprogs
     usbutils # usb tools
   ];

  nixpkgs.config.allowUnfree = true;
  nix = {
    package = pkgs.nixFlakes;
    extraOptions = "experimental-features = nix-command flakes";
    registry.nixpkgs.flake = inputs.nixpkgs;
    settings.auto-optimise-store = true;
    gc = {
      automatic = true;
      dates = "weekly";
      options = "--delete-older-than 7d";
    };
  };

  # some theming -- should make a module out of this but can't be bothered rn
  fonts = {
    fonts = with pkgs; [
      fira-code
      fira-code-symbols
      siji
      font-awesome
      (iosevka-bin.override { variant = "aile"; })
      (iosevka-bin.override { variant = "etoile"; })
      (nerdfonts.override { fonts = [ "JetBrainsMono" "DroidSansMono" "IBMPlexMono" ]; })
    ];
    fontconfig.defaultFonts = {
      monospace = [ "JetBrainsMono Nerd Font Mono" ];
      serif = [ "Iosevka Etoile" ];
      sansSerif = [ "Iosevka Aile" ];
    };
  };

  services.picom = {
    enable = true;
    fade = true;
    fadeDelta = 1;
    fadeSteps = [ 0.01 0.012 ];
    shadow = true;
    shadowOffsets = [ (-10) (-10) ];
    shadowOpacity = 0.22;
    # activeOpacity = "1.00";
    # inactiveOpacity = "0.92";
    settings = {
      shadow-radius = 12;
      # blur-background = true;
      # blur-background-frame = true;
      # blur-background-fixed = true;
      blur-kern = "7x7box";
      blur-strength = 320;
    };
  };

  system.stateVersion = "22.05";
}
